{ docker-nixpkgs
, nixFlakes
, bash
, jq
, gnugrep
}:
docker-nixpkgs.cachix.override {
  nix = nixFlakes;
  extraContents = [ bash jq gnugrep ];
}
