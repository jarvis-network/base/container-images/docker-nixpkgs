{ docker-nixpkgs
, cachix
, nix
, extraContents ? []
}:
(docker-nixpkgs.nix.override {
  nix = nix;
  extraContents = [ cachix ] ++ extraContents;
}).overrideAttrs (prev: {
  meta = (prev.meta or {}) // {
    description = "Nix and Cachix image";
  };
})
