{ docker-nixpkgs
, nixFlakes
, extraContents ? []
}:
docker-nixpkgs.cachix.override {
  nix = nixFlakes;
  extraContents = extraContents;
}
